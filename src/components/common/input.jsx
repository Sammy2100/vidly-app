import React from "react";
import { FormGroup, Label, Input as RsInput } from "reactstrap";

const Input = ({ name, label, error, focused, ...rest }) => {
  return (
    <FormGroup>
      <Label for={name}>{label}</Label>
      {/* <RsInput
        // innerRef={name}
        autoFocus={!!focused}
        type={type}
        id={name}
        name={name}
        placeholder={`Enter ${label}`}
        value={value}
        onChange={onChange}
      /> */}
      <RsInput
        {...rest}
        name={name}
        id={name}
        placeholder={`Enter ${label}`}
        autoFocus={!!focused}
      />
      <span className="text text-danger">{error}</span>
    </FormGroup>
  );
};

export default Input;
