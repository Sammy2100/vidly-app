import React from "react";
import { Input, FormGroup, Label } from "reactstrap";

const Select = ({ name, label, options, error, ...rest }) => {
  return (
    <FormGroup>
      <Label for={name}>{label}</Label>
      <Input type="select" id={name} name={name} {...rest}>
        <option value="" />
        {options.map(option => (
          <option key={option._id} value={option._id}>
            {option.name}
          </option>
        ))}
      </Input>

      {error && <span className="text text-danger">{error}</span>}
    </FormGroup>
  );
};

export default Select;
