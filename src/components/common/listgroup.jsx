import React from "react";
import { ListGroup, ListGroupItem } from "reactstrap";

const LstGrp = props => {
  const {
    items,
    textProperty,
    valueProperty,
    onItemSelect,
    selectedItem
  } = props;
  return (
    <ListGroup>
      {items.map(item => (
        <ListGroupItem
          key={item[valueProperty]}
          onClick={() => onItemSelect(item)}
          active={item === selectedItem}
          className="clickable"
        >
          {item[textProperty]}
        </ListGroupItem>
      ))}
    </ListGroup>
  );
};

LstGrp.defaultProps = {
  textProperty: "name",
  valueProperty: "_id"
};

export default LstGrp;
