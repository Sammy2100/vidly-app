import React from "react";
import { Input } from "reactstrap";

const SearchBox = ({ value, onChange }) => {
  return (
    <Input
      type="text"
      name="query"
      placeholder="Search..."
      className="my-3"
      value={value}
      onChange={e => onChange(e.currentTarget.value)}
    />
  );
};

export default SearchBox;
