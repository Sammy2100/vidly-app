import React from "react";
import { Redirect } from "react-router-dom";
import { Form as RsForm, Col, Row } from "reactstrap";
import Joi from "joi-browser";
import Form from "./common/form";
import auth from "../services/authService";

class LoginForm extends Form {
  //   username = React.createRef();
  //   componentDidMount() {
  //     this.username.current.focus();
  //   }

  state = {
    data: { username: "", password: "" },
    errors: {}
  };

  schema = {
    username: Joi.string()
      .required()
      .label("Username"),
    password: Joi.string()
      .required()
      .label("Password")
  };

  doSubmit = async () => {
    try {
      const { username, password } = this.state.data;
      await auth.login(username, password);
      const { state } = this.props.location;
      window.location = state ? state.from.pathname : "/";
      //this.props.history.push("/");
    } catch (ex) {
      if (ex.response && ex.response.status === 400) {
        const errors = { ...this.state.errors };
        errors.username = ex.response.data;
        this.setState({ errors });
      }
    }
  };

  render() {
    if (auth.getCurrentUser()) return <Redirect to="/" />;
    return (
      <div>
        <Row>
          <Col xs="3"></Col>
          <Col xs="6">
            <h1>Login</h1>
            <RsForm onSubmit={this.handleSubmit}>
              {this.renderInput("username", "Username", "", true)}
              {this.renderInput("password", "Password", "password")}
              {this.renderButton("Login")}
            </RsForm>
          </Col>
          <Col xs="3"></Col>
        </Row>
      </div>
    );
  }
}

export default LoginForm;
