import * as Sentry from "@sentry/browser";

function init() {
  Sentry.init({
    dsn: "https://54df24f53d3648fc8e525760c588ed0d@sentry.io/1798411"
  });
}

function log(error) {
  Sentry.captureException(error);
}

export default { init, log };
