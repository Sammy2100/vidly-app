import jwdDecode from "jwt-decode";
import http from "./httpService";

const authApi = "/auth";
const tokenKey = "token";

http.setJwt(getJwt());

export async function login(email, password) {
  const login = { email: email, password: password };
  const { data: jwt } = await http.post(authApi, login);
  localStorage.setItem(tokenKey, jwt);
}

export function loginWithJwt(jwt) {
  localStorage.setItem(tokenKey, jwt);
}

export function logout() {
  //console.log(localStorage.getItem(tokenKey));
  localStorage.removeItem(tokenKey);
}

export function getCurrentUser() {
  try {
    const jwt = localStorage.getItem(tokenKey);
    return jwdDecode(jwt);
  } catch (ex) {
    return null;
  }
}

export function getJwt() {
  //console.log(localStorage.getItem(tokenKey));
  return localStorage.getItem(tokenKey);
}

export default {
  login,
  loginWithJwt,
  logout,
  getCurrentUser,
  getJwt
};
