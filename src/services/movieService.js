import http from "./httpService";

const moviesApi = "/movies";

function movieUrl(id) {
  if (id) return `${moviesApi}/${id}`;

  return moviesApi;
}

export function getMovies() {
  return http.get(moviesApi);
}

export function getMovie(movieId) {
  return http.get(movieUrl(movieId));
}

export function saveMovie(movie) {
  if (movie._id) {
    const data = { ...movie };
    delete data._id;
    return http.put(movieUrl(movie._id), data);
  }

  return http.post(movieUrl(), movie);
}

export function deleteMovie(movieId) {
  return http.delete(movieUrl(movieId));
}
