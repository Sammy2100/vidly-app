import http from "./httpService";

const userApi = "/users";

export function register(user) {
  const newUser = {
    email: user.username,
    password: user.password,
    name: user.name
  };
  return http.post(userApi, newUser);
}
